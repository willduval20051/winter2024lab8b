import java.util.Random;
public class Board{
	private Tile[][] grid;
	private final int SIZE = 5;
	
	public Board(){
		Random rng = new Random();
		this.grid = new Tile[SIZE][SIZE];
		for(int i = 0; i < this.grid.length; i++){
			int randIndex = rng.nextInt(this.grid[i].length);
			for(int j = 0; j < this.grid[i].length; j++){
				if(j == randIndex){
					this.grid[i][j] = Tile.HIDDEN_WALL;
					
				}
				else{
					this.grid[i][j]= Tile.BLANK;
				}
			}			
			
		}
		
	}
	
	public String toString(){
		
		String board = "";
		for(int i = 0; i < this.grid.length; i++){
			for(int j = 0; j < this.grid[i].length; j++){
				board += this.grid[i][j] + "   ";
			}			
			board += "\n";
		}
		return board;
		
	}
	
	public int placeToken(int row, int col){
		
		if(row < SIZE && col < SIZE){
			if(this.grid[row][col] == Tile.BLANK){
				this.grid[row][col] = Tile.CASTLE;
				return 0;
			}
			else if(this.grid[row][col] == Tile.CASTLE || this.grid[row][col] == Tile.WALL){
				return -1;
			}
			else{
				this.grid[row][col] = Tile.WALL;
				return 1;
			}
		}
		else{
			return -2;
		}
		
	}
	

	
	
	
}