import java.util.Scanner;
public class BoardGameApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Board boardGame = new Board();
		System.out.println(Tile.WALL.getName());
		
		System.out.println("Welcome Players too The BOARD GAME, are you ready to have some fun");
		
		int numCastles = 5;
		int turns = 0;
		while(numCastles > 0 && turns < 8){
		
		
			System.out.println(boardGame.toString());
			System.out.println("Your Current Number of Castle Tokens: " + numCastles);
			System.out.println("The Current Turn: " + turns);
			System.out.println("Please enter the row number(0-4) and the column number(0-4) of where you want to place your castle token.");
			int row = reader.nextInt();
			int col = reader.nextInt();
			int result = boardGame.placeToken(row, col);
			if(result == -1 || result == -2){
				
				System.out.println("Please re-enter the row number(0-4) and the column number(0-4) of where you want to place your castle token.");
				row = reader.nextInt();
				col = reader.nextInt();
				result = boardGame.placeToken(row, col);
			}
			else if(result == 1){
				
				System.out.println("Oops! There was a WALL in that position.");
				turns += 1;
				
			}
			else{
				System.out.println("You've successfully placed your CASTLE tile.");
				numCastles--;
				turns += 1;
			}
			
		}
		
		if(numCastles == 0){
			System.out.println("Congratualtions you've won the game.");
		}
		else{
			System.out.println("Sorry, you've sadly lost the game.");
		}
		
	}
	
	
	
	
	
	
	
}